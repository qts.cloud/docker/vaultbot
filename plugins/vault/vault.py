#!/usr/bin/env python

import os
import string
import random
import hvac
from errbot import BotPlugin, botcmd, arg_botcmd


vault_addr = os.environ.get('VAULT_ADDR', None)
vault_token = os.environ.get('VAULT_TOKEN', None)

client = hvac.Client(url=vault_addr)
client.token = vault_token

def randomString(stringLength):
    """ Generate a random string with the combination of lowercase and uppercase letters """
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))


def adduser(username=None, password=None, stage=None, path=None):
    if not username:
        return '! --username is required'

    if not password:
        password = randomString(32)
    path = f"{path}/{username}"

    token = wrap(password, 600)['wrap_info']['token']
    url = f"http://otp.bannersnack.net/decode/{token}"
    kwargs = {
        "password": f"{password}",
        "policies": stage
    }
    try:
        client.write(path, None, **kwargs)
        result = f"Created user **{username}**. Passord: **{url}**"
    except hvac.exceptions.InvalidRequest as error:
        result = f"Failed to create user: {username}"
    return result

def deluser(username=None, stage=None, path=None):
    if not username:
        return '! --username is required'
    path = f"{path}/{username}"
    try:
        client.delete(path)
        result = f"Deleted user **{username}**"
    except hvac.exceptions.InvalidRequest as error:
        result = f"Failed to delete user: {username}"
    return result

def wrap(secret, ttl):
    wrap_ttl = f"{int(ttl / 60)}m"
    try:
        result = client.write(
            path="sys/wrapping/wrap",
            wrap_ttl=wrap_ttl,
            secret=secret,
        )
    except hvac.exceptions.InvalidRequest as error:
        print(f"Error encoding secret: {error}")
        result = {"wrap_info": {"token": f"Error: {error}"}}
    return result

def unwrap(token):
    try:
        result = client.sys.unwrap(
            token=token,
        )
    except hvac.exceptions.InvalidRequest as error:
        print(f"Error decoding token `{token}`: {error}")
        result = {"data": {"secret": "Error: Token invalid/expired"}}
    return result


class Vault(BotPlugin):
    """
    Hashicorp Vault
    * Encode:
      /vault encode This is my super secret stuff
    * Decode:
      /vault decode s.MV9m6Xqn1TkYlsTbATvcn1ry
    * OpenVPN:
      /openvpn adduser --username john.doe
      /openvpn deluser --username john.doe
    """

    @arg_botcmd('message', type=str, nargs='*')
    @arg_botcmd('--ttl', dest='ttl', type=int, default=120)
    def vault_encode(self, msg, message=None, ttl=None):
        secret = " ".join(message)
        if ttl < 60:
            ttl = 60
        result = wrap(secret, ttl)
        return f"Your token is: **{result['wrap_info']['token']}**"

    @arg_botcmd('token', type=str)
    def vault_decode(self, msg, token=None):
        result = unwrap(token)
        return f"Secret is: **{result['data']['secret']}**"

    @arg_botcmd('username', type=str)
    @arg_botcmd('--password', dest='password', type=str)
    @arg_botcmd('--stage', dest='stage', type=str, default='lotus')
    @arg_botcmd('--path', dest='path', type=str, default='auth/openvpn/users')
    def openvpn_adduser(self, msg, username, password=None, stage=None, path=None):
        result = adduser(username, password, stage, path)
        return result

    @arg_botcmd('username', type=str)
    @arg_botcmd('--stage', dest='stage', type=str, default='lotus')
    @arg_botcmd('--path', dest='path', type=str, default='auth/openvpn/users')
    def openvpn_deluser(self, msg, username, stage=None, path=None):
        result = deluser(username, stage, path)
        return result
