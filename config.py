import os
import logging

# This is a minimal configuration to get you started with the Text mode.
# If you want to connect Errbot to chat services, checkout
# the options in the more complete config-template.py from here:
# https://raw.githubusercontent.com/errbotio/errbot/master/errbot/config-template.py

# BACKEND = 'Text'
BACKEND = os.environ.get('BACKEND', 'Telegram')
# AUTOINSTALL_DEPS = False

BOT_DATA_DIR = r'./data'
BOT_EXTRA_PLUGIN_DIR = r'./plugins'

BOT_LOG_FILE = r'./errbot.log'
BOT_LOG_LEVEL = logging.DEBUG

BOT_PREFIX = os.environ.get('BOT_PREFIX', '/')
BOT_ALT_PREFIXES = ('Err', '@bot', '@Err', '@err')
BOT_ALT_PREFIX_SEPARATORS = (':', ',', ';')

if BACKEND == 'Text':
    BOT_ADMINS = ('@Iulian',)
    VAULT_ADMINS = ('@Iulian',)
else:
    BOT_ADMINS = tuple(os.environ.get('ADMINS', "").split())
    VAULT_ADMINS = tuple(os.environ.get('VAULT_ADMINS', "").split())

BOT_IDENTITY = {
    'token': os.environ.get('TELEGRAM_TOKEN', None)
}
CHATROOM_PRESENCE = ()

# ACCESS_CONTROLS_DEFAULT = {}

ACCESS_CONTROLS = {
    'ChatRoom:*': {'allowusers': ()},
    'Flows:*': {'allowusers': BOT_ADMINS},
    'Health:*': {'allowusers': BOT_ADMINS},
    'Plugins:*': {'allowusers': BOT_ADMINS},
    'Utils:whoami': {'denyusers': ()},
    'Utils:*': {'allowusers': BOT_ADMINS},
    'Help:*': {'denyusers': ()},
    'Vault:*': {'allowusers': VAULT_ADMINS},
}

HIDE_RESTRICTED_ACCESS = True
HIDE_RESTRICTED_COMMANDS = True
DIVERT_TO_PRIVATE = ('whoami', 'vault_encode', 'vault_decode')

COMPACT_OUTPUT = True
MESSAGE_SIZE_LIMIT = 20000
BOT_PLUGIN_INDEXES = 'tools/repos.json'
