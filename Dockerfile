FROM python:alpine

WORKDIR /app

# Add files
ADD . .

# Install requirements
RUN apk update
RUN apk add --no-cache --virtual build-dependencies gcc py-pip libffi-dev build-base openssl-dev 
#  python3-dev

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

CMD ["errbot"]